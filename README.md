# Angular7 + NZ-ZORRO Demo (Web Course Project)

## Project Presentation

访问 [angular7-demo](https://xiaofeit.gitlab.io/angular7-demo) 查看项目的实时预览

> 请使用 http 访问，如果登陆失败，无法加载数据，请点击浏览器的允许从不安全的站点加载数据

## Back-end

### Local Json-Server

> 需要 node、npm 环境

```bash
npm install -g json-server
cd <project-path>
cd back-end
npm run mock
```

### Json-Servce On Server

> 使用服务器上的 Json-Server 

1. Visit [http://123.206.80.115:3000/db](http://123.206.80.115:3000/db), get all data.

2. Visit [http://123.206.80.115:3000/user](http://123.206.80.115:3000/users), get all users.

3. And so on.

### Change back-end

- Form local Json-Server to Json-Server on server, change the `baseUrl` in file `fron-end/services/config.service.ts` to `http://123.206.80.115:3000`. 

- Form Json-Server on server to local Json-Server,  change the `baseUrl` in file `fron-end/services/config.service.ts` to  `http://localhost:3000`。
