# 后台说明

使用 JSON-Server 的 Restful 接口来模拟后台数据。

## 使用方法

1. 全局安装 `JSON Server`.

    `npm install -g json-server`。

2. 运行，在当前目录下执行命令 `npm run mock`。

3. 访问 `http://localhost:3000` 即可访问后台服务。

> json-server 使用教程 [https://www.cnblogs.com/ys-wuhan/p/6387791.html](https://www.cnblogs.com/ys-wuhan/p/6387791.html)

## 数据说明

1. users => 所有用户

2. leaves => 所有请假申请记录

3. outs => 所有外出申请记录

4. records => 所有打卡记录

## 接口说明

1. All
    
    - GET, http://localhost:3000/db, return all data.

2. Users 
    
    - GET, http://localhost:3000/users, return all users.
    - GET, http://localhost:3000/users/{id}, return this id user.
    - POST, http://localhost:3000/users, add a new user record.
    - PUT, http://localhost:3000/users/{id}, change the user's record.
    - DELETE, http://localhost:3000/user/{id}, delete a user.

2. 其他接口类推