import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  formatTime(time: Date): String {
    let year = time.getFullYear();
    let month = time.getMonth() + 1;
    let day = time.getDate();
    let week = time.getDay() + 1;
    let hour = time.getHours();
    let minuts = time.getMinutes();
    let second = time.getSeconds();
    return `${year}年${month}月${day}日 ${hour}:${minuts}:${minuts} (星期 ${week})`;
  }
}
