import {Injectable} from '@angular/core';

/**
 * @description 配置类
 * @date 2018-12-18
 * @author Wu Kexin
 * @export
 * @class ConfigService
 */
@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  /**
   * @description 配置网站使用的后台
   * @param "" / "default", 使用远程 JSON-SERVER
   * @param "bomb", 使用 bmob 后台
   * @param "local", 使用本地 JSON-SERVER
   * @summary 注意切换不同的后台，数据可能因为后台的不同步而导致不同
   * @type {string}
   * @memberof ConfigService
   */
  remote: string = "";

  // JSON-SERVE 后台地址
  baseUrl: string;
  userUrl: string;
  outUrl: string;
  leaveUrl: string;
  recordUrl: string;
  loginUrl: string = "";  // It works only when the remote is setted to bmob

  /**
   * @description 使用 bmob 后台时候需要时设置特殊的 request headers
   * @type {Object}
   * @memberof ConfigService
   */
  headers: Object = {
    "Content-Type": "application/json",
  };

  constructor() {
    if (this.remote === "local") {
      this.baseUrl = "http://localhost:3000";
      this.userUrl = `${this.baseUrl}/users`;
      this.outUrl = `${this.baseUrl}/outs`;
      this.leaveUrl = `${this.baseUrl}/leaves`;
      this.recordUrl = `${this.baseUrl}/records`;
    } else if (this.remote === "bmob") {
      this.baseUrl = "https://api2.bmob.cn/1/classes";
      this.userUrl = `https://api2.bmob.cn/1/users`;
      this.loginUrl = `https://api2.bmob.cn/1/login`;
      this.outUrl = `${this.baseUrl}/out`;
      this.leaveUrl = `${this.baseUrl}/leave`;
      this.recordUrl = `${this.baseUrl}/record`;
      this.headers = {
        "Content-Type": "application/json",
        "X-Bmob-Application-Id": "96dbe8c000a017883f86f5832750981b",
        "X-Bmob-REST-API-Key": "9d5262dafcdb5d4be3165c8b59ec6028",
      };
    } else if (this.remote === "default") {
      this.baseUrl = "http://123.206.80.115:3000";
      this.userUrl = `${this.baseUrl}/users`;
      this.outUrl = `${this.baseUrl}/outs`;
      this.leaveUrl = `${this.baseUrl}/leaves`;
      this.recordUrl = `${this.baseUrl}/records`;
    } else {
      this.baseUrl = "http://123.206.80.115:3000";
      this.userUrl = `${this.baseUrl}/users`;
      this.outUrl = `${this.baseUrl}/outs`;
      this.leaveUrl = `${this.baseUrl}/leaves`;
      this.recordUrl = `${this.baseUrl}/records`;
    }
  }
}
