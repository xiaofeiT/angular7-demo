import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AuthGuard } from "../auth/auth.guard";
import { UserService } from "./user.service";
import { ConfigService } from "./config.service";
import { NzMessageService } from 'ng-zorro-antd';
import { User } from "../domain/user";

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isLoggedIn = false;
  userLogined: User;

  redirectUrl: string;  //登陆成功后访问的路径

  constructor(
    private userService: UserService,
    private configService: ConfigService,
    private message: NzMessageService,
    private router: Router,
    private authGuard: AuthGuard,
  ) {
    if (localStorage.getItem("userId") || sessionStorage.getItem("userId")) {
      let userId = localStorage.getItem("userId") ? localStorage.getItem("UserId") : sessionStorage.getItem("userId");
      this.userService.getUserById(userId).subscribe((res) => {
        this.userLogined = res;
        this.isLoggedIn = true;
        authGuard.publicLogin(true);
      })
    }
  };

  /**
   * @description login function
   * @author Sun Pengfei
   * @date 2018-12-28
   * @param {string} username
   * @param {string} password
   * @param {boolean} rememberMe
   * @memberof AuthService
   */
  login(username: string, password: string, rememberMe: boolean) {
    this.userService.userLogin(username, password).subscribe((data) => {
      this.message.remove();
      let redirectUrl = (localStorage.getItem('redirectUrl') === null) ? '/' : localStorage.getItem('redirectUrl');
      // 由于 JSON-SERVER 和 BMOB 后台登陆验证方式不同，所以对返回的数据采用不同的处理
      if (this.configService.remote === "bmob") {
        let user = data;
        if (undefined === user["sessionToken"]) {
          this.message.info(user[""]);
        } else {
          this.loginSuccessCallback(user, rememberMe, redirectUrl);
        }
      } else {
        let user = data[0];
        if (null === user || undefined === user || password !== user.password) {
          this.message.info("用户名/密码错误");
        } else if (password === user.password) {
          this.loginSuccessCallback(user, rememberMe, redirectUrl);
        } else {
          this.message.info("网络出错，请重试");
        }
      }
    })
  }

  /**
   * @description logout function
   * @author Sun Pengfei
   * @date 2018-12-28
   * @memberof AuthService
   */
  logout(): void {
    this.isLoggedIn = false;
    this.userLogined = null;
    // TODO: 存储当前URL，注销后重新登陆仍然返回当前URL
    sessionStorage.removeItem("userId");
    sessionStorage.removeItem("username");
    localStorage.removeItem("userId");
    localStorage.removeItem("username");
    this.router.navigate(["/login"]);
  }

  /**
   * @description 登陆成功后的执行的函数
   * @author Sun Pengfei
   * @date 2018-12-28
   * @param {User} user
   * @param {boolean} rememberMe
   * @param {String} redirectUrl
   * @returns
   * @memberof AuthService
   */
  loginSuccessCallback(user: User, rememberMe: boolean, redirectUrl: String) {
    this.isLoggedIn = true;
    this.userLogined = user;
    this.router.navigate([redirectUrl]);
    this.authGuard.publicLogin(true);
    this.message.remove();
    this.message.info("登陆成功");
    if (rememberMe) {
      localStorage.setItem("userId", user.id.toString());
      localStorage.setItem("username", user.username);
      localStorage.setItem("sessionToken", user["sessionToken"]);
      sessionStorage.setItem("userId", user.id.toString());
      sessionStorage.setItem("username", user.username);
      sessionStorage.setItem("sessionToken", user["sessionToken"]);
    } else {
      sessionStorage.setItem("userId", user.id.toString());
      sessionStorage.setItem("username", user.username);
      sessionStorage.setItem("sessionToken", user["sessionToken"]);
    }
    return true;
  }
}
