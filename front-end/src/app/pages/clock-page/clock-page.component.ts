import { Component, OnInit } from '@angular/core';
import { NzMessageService } from "ng-zorro-antd";
import { Record } from "../../domain/record";
import { AuthService } from "../../services/auth.service";
import { AuthGuard } from "../../auth/auth.guard";
import { RecordService } from "../../services/record.service";
import { UtilsService } from "../../utils/utils.service";
import { Subscription } from 'rxjs';
import { User } from "../../domain/user";

// * @author Song Qiqi
@Component({
  selector: 'app-clock-page',
  templateUrl: './clock-page.component.html',
  styleUrls: ['./clock-page.component.less']
})
export class ClockPageComponent implements OnInit {

  records: Record[];
  mood: string = "啷里格啷啦啦啦";

  login: any;
  notLogin: boolean = true;
  subscription: Subscription;
  currentUser: User;
  currentUserPosition: boolean = false;

  constructor(
    private authService: AuthService,
    private recordService: RecordService,
    private message: NzMessageService,
    private authGuard: AuthGuard,
    private utils: UtilsService,
  ) {
    this.subscription = this.authGuard.loginStatusObservable.subscribe((data) => {
      this.login = data;
      this.notLogin = !data;
      this.currentUser = this.authService.userLogined;
      this.loadRecords();
    });
  }

  ngOnInit() {
    this.login = this.authGuard.isLogin();
    this.notLogin = !this.authGuard.isLogin();
    this.message.info("加载数据中");
    this.loadRecords();
  }

  /**
   * @description 加载打卡记录数据
   * @author Sun Pengfei
   * @date 2018-12-27
   * @memberof ClockPageComponent
   */
  loadRecords() {
    let userId = localStorage.getItem("userId") ? localStorage.getItem("userId") : sessionStorage.getItem("userId");
    this.recordService.getRecordByUserId(userId).subscribe((res) => {
      this.message.remove();
      this.message.success("加载成功");
      this.records = res.reverse();
    });
  }

  /**
   * @description 上班打卡点击事件
   * @author Sun Pengfei
   * @date 2018-12-27
   * @memberof ClockPageComponent
   */
  inClock() {
    let userId = this.authService.userLogined.id;
    let record: Record = {
      id: this.recordService.createNewRecordId(),
      userId: userId,
      time: new Date(),
      mood: this.mood,
      isLate: false,
      isEarly: false,
    };
    if (record.time.getHours() > 8) {
      record.isLate = true;
    } else {
      record.isLate = false;
    }
    // http request
    this.recordService.createRecord(record).subscribe(() => {
      this.message.success("打卡成功");
      this.recordService.getRecordByUserId(userId).subscribe((res) => {
        this.records = res.reverse();
      })
    });
  }

  /**
   * @description 下班打卡点击事件
   * @author Sun Pengfei
   * @date 2018-12-27
   * @memberof ClockPageComponent
   */
  outClock() {
    let userId = this.authService.userLogined.id;
    let record: Record = {
      id: this.recordService.createNewRecordId(),
      userId: userId,
      time: new Date(),
      mood: this.mood,
      isLate: false,
      isEarly: false,
    };
    if (record.time.getHours() < 12) {
      if (record.time.getHours() > 8) {
        record.isLate = true;
      } else {
        record.isLate = false;
      }
    } else {
      if (record.time.getHours() < 18) {
        record.isEarly = true;
      } else {
        record.isEarly = false;
      }
    };
    // http request
    this.recordService.createRecord(record).subscribe(() => {
      this.message.success("打卡成功");
      this.recordService.getRecordByUserId(userId).subscribe((res) => {
        this.records = res.reverse();
      })
    });
  }

  /**
   * @description 通过调用 utilService 中的 formatTime function 格式化时间
   * @author Sun Pengfei
   * @date 2018-12-27
   * @param {Date} time
   * @memberof ClockPageComponent
   */
  formatTime(time: Date) {
    let date = new Date(time);
    return this.utils.formatTime(date);
  }

}
