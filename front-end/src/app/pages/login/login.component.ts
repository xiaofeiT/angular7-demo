import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { NzMessageService } from "ng-zorro-antd";

// * @author Fan Lishui
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  username: string = "";
  password: string = "";
  rememberLogin: boolean = false;
  usernameError: boolean = false;
  usernameErrorMessage: string = "dfdfdf";

  constructor(
    private authService: AuthService,
    private message: NzMessageService,
  ) { 
    document.onkeydown = (event) => {
      if(event.keyCode === 13) {
        this.login();
      }
    }
  }

  ngOnInit() {
  }

  login(): void {
    this.message.remove();
    if (this.username === "") {
      this.message.info("用户名不能为空！");
    } else  if (this.password === "") {
      this.message.info("密码不能为空！");
    } else {
      this.message.info("正在登陆");
      this.authService.login(this.username, this.password, this.rememberLogin);
    }
  }

  forgetPassword() {
    this.message.info("该功能暂未实现")
  }

}
