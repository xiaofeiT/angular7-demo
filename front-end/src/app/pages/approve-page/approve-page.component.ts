import { Component, OnInit } from '@angular/core';
import { Leave } from '../../domain/leave';
import { Out } from "../../domain/out";
import { OutService } from "../../services/out.service";
import { LeaveService } from "../../services/leave.service";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-approve-page',
  templateUrl: './approve-page.component.html',
  styleUrls: ['./approve-page.component.less']
})
export class ApprovePageComponent implements OnInit {

  outs: Out[] = [];
  unCompletedApproveOut: Out[] = [];
  completedApprovedOut: Out[] = [];
  unCompletedOutCount: number = 0;
  hasUncompletedApprovelOut: boolean = false;

  leaves: Leave[] = [];
  unCompletedApproveLeave: Leave[] = [];
  completedApprovedLeave: Leave[] = [];
  unCompletedLeavdCount: number = 0;
  hasUncompletedApprovelLeave: boolean = false;

  isVisible: boolean = false;
  isLoading: boolean = false;

  currentItemType: string = ""; // 当前操作的记录类型，1 =》 out， 2 =》 leave
  currentItem: any = {};  // 当前操作的某条记录
  currentOperation: string = "";  // 当前操作
  reply: string = ""; // 审批回复

  subscrition: Subscription;

  constructor(
    private outService: OutService,
    private leaveService: LeaveService,
  ) {
    this.subscrition = this.outService.hasNewAOutObservable.subscribe(data => {
      if (data) {
        if (this.currentItemType === "out") {
          this.getOuts();
        } else {
          this.getLeaves();
        }
      }
    });
  }

  ngOnInit() {
    this.getLeaves();
    this.getOuts();
  }

  /**
   * @description 获取所有 out 并分类
   * @author Sun Pengfei
   * @date 2019-01-01
   * @memberof ApprovePageComponent
   */
  getOuts(): void {
    this.outs = [];
    this.unCompletedApproveOut = [];
    this.completedApprovedOut = [];
    this.outService.getOuts().subscribe(outs => {
      this.outs = outs;
      for (let i = 0; i < this.outs.length; i++) {
        if (this.outs[i].state === 1) {
          this.unCompletedApproveOut.push(this.outs[i]);
          this.unCompletedOutCount++;
        } else {
          this.completedApprovedOut.push(this.outs[i]);
        }
        ;
        if (this.unCompletedOutCount > 0) {
          this.hasUncompletedApprovelOut = true;
        }
      }
    });
  }

  /**
   * @description 获取所有 leaves 并分类
   * @author Sun Pengfei
   * @date 2019-01-01
   * @memberof ApprovePageComponent
   */
  getLeaves(): void {
    this.leaves = [];
    this.unCompletedApproveLeave = [];
    this.completedApprovedLeave = [];
    this.leaveService.getLeaves().subscribe(leaves => {
      this.leaves = leaves;
      for (let i = 0; i < this.leaves.length; i++) {
        if (this.leaves[i].state === 1) {
          this.unCompletedApproveLeave.push(this.leaves[i]);
          this.unCompletedLeavdCount++;
        } else {
          this.completedApprovedLeave.push(this.leaves[i]);
        }
        ;
        if (this.unCompletedLeavdCount > 0) {
          this.hasUncompletedApprovelLeave = true;
        }
      }
    }
    )
  }

  /**
   * @description 鼠标悬浮显示“通过”、“驳回”按钮
   * @author Sun Pengfei
   * @date 2018-12-31
   * @param {string} className
   * @param {number} index
   * @memberof ApprovePageComponent
   */
  mouseHover(className: string, index: number) {
    let buttonDiv = document.getElementsByClassName(className)[index];
    buttonDiv["style"].display = "block";
  }

  /**
   * @description 鼠标移出范围“通过”、“驳回”按钮消失
   * @author Sun Pengfei
   * @date 2018-12-31
   * @param {string} className
   * @param {number} index
   * @memberof ApprovePageComponent
   */
  mouseDown(className: string, index: number) {
    let buttonDiv = document.getElementsByClassName(className)[index];
    buttonDiv["style"].display = "none";
  }

  /**
   * @description 记录当前的操作相关数据，显示审批回复弹层
   * @author Sun Pengfei
   * @date 2018-12-31
   * @param {string} itemType
   * @param {*} item
   * @param {string} operation
   * @memberof ApprovePageComponent
   */
  showLayer(itemType: string, item: any, operation: string) {
    this.currentItemType = itemType;
    this.currentItem = item;
    this.currentOperation = operation;
    this.isVisible = true;
  }

  /**
   * @description 弹层取消
   * @author Sun Pengfei
   * @date 2018-12-31
   * @memberof ApprovePageComponent
   */
  handleCancel() {
    this.isVisible = false;
  }

  /**
   * @description 弹层填写审批回复确定，包括 out 和 leave，网络请求，重置回复输入框
   * @author Sun Pengfei
   * @date 2018-12-31
   * @memberof ApprovePageComponent
   */
  handleOk() {
    this.isLoading = true;
    this.currentItem["approve_reason"] = this.reply;
    this.reply = "";
    if (this.currentOperation === "approve") {
      this.currentItem["state"] = 2;
    } else {
      this.currentItem["state"] = 3;
    };
    if (this.currentItemType === "out") {
      this.outService.updateOut(this.currentItem).subscribe(res => {
        if (res.state !== 1) {
          this.isLoading = false;
          this.isVisible = false;
          this.outService.publicNeedFresh(true);
        }
      });
    } else if (this.currentItemType === "leave") {
      this.leaveService.updateLeave(this.currentItem).subscribe(res => {
        if (res.state !== 1) {
          this.isLoading = false;
          this.isVisible = false;
          this.leaveService.publicLeavesNeedFresh(true);
        }
      });
    } else { };
  }
}
