import { Component, Input, OnInit } from '@angular/core';
import { Out } from 'src/app/domain/out';
import { UserService } from "../../services/user.service";
import { UtilsService } from "../../utils/utils.service";

@Component({
  selector: 'app-goout-page-card',
  templateUrl: './goout-page-card.component.html',
  styleUrls: ['./goout-page-card.component.less']
})
export class GooutPageCardComponent implements OnInit {

  @Input() out: Out;
  // 根据输入的属性计算得到的值
  username: String; // 此条记录申请人的用户名
  className: String; // 此条记录的类名，用来显示不同的颜色

  constructor(
    private userService: UserService,
    private utils: UtilsService,
  ) { }

  ngOnInit() {
    if (this.out.state === 1) this.className = "out-wait";
    else if (this.out.state === 2) this.className = "out-completed";
    else this.className = "out-rejected";
    this.userService.getUserById(this.out.userId).subscribe( res => {
      this.username = res.username;
    });
  }

  formatTime(time: any) {
    let date = new Date(time);
    return this.utils.formatTime(date);
  }

}
